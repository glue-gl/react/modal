# modal

A simple solution for flexible modals on React applications.

## Usage

To start using Modal, first make sure to add a `<ModalWrapper>` around all your modal-using components.
This works similarly to the Redux or React-Intl providers.
You should also add a `<Modal>` where you want your modals to appear.

```js
import { ModalWrapper, Modal } from '@gluedigital/modal'

const App = () =>
  <ModalWrapper>
    {/* Your other components here... */}
    <Modal />
  </ModalWrapper>
```

To register a modal type, just call `registerModal`.

```js
import { registerModal } from '@gluedigital/modal'

const DemoModal = () => <h1>This is a modal</h1>

registerModal('demo', DemoModal)
```

To show a modal, the simplest way is using a ModalLink.

```js
import { ModalLink } from '@gluedigital/modal'

const DemoPage = () =>
  <div>
    Press the button to open the modal:
    <ModalLink modal="demo">Click here</ModalLink>
  </div>
```

If you need to control the modal programmatically, you can also use the `useModal` hook, and call any of the methods on the returned object (including `show()` and `hide()`).

```js
import { useModal } from '@gluedigital/modal'

const DemoComponent = () => {
  const modal = useModal()
  const handleSave = () => {
    modal.show('loading')
    doSomethingAsync()
      .then(() => modal.hide())
  }
  return <button onClick={handleSave}>Save</button>
}
```
