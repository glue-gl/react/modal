import React, { useContext, useReducer, useRef } from 'react'
import PropTypes from 'prop-types'

const ModalContext = React.createContext()
const types = {}

export const registerModal = (name, component) => {
  types[name] = component
}

const reducer = (state, { type, modal = null, options = null }) => {
  switch (type) {
    case 'show':
      return { modal, options }
    case 'hide':
      return { modal: null, options: null }
    default:
      return state
  }
}

export const ModalWrapper = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, { modal: null, options: null })
  const ctx = useRef({
    show: (modal, options) => dispatch({ type: 'show', modal, options }),
    hide: () => dispatch({ type: 'hide' })
  })
  Object.assign(ctx.current, state)
  return (
    <ModalContext.Provider value={[ctx.current]}>
      {children}
    </ModalContext.Provider>
  )
}
ModalWrapper.propTypes = {
  children: PropTypes.node
}

export const ManualModalWrapper = ({ children, ctx }) => {
  return (
    <ModalContext.Provider value={ctx}>
      {children}
    </ModalContext.Provider>
  )
}
ManualModalWrapper.propTypes = {
  children: PropTypes.node,
  ctx: PropTypes.shape({
    show: PropTypes.func.isRequired,
    hide: PropTypes.func.isRequired,
    modal: PropTypes.string
  })
}

export const Modal = ({ children }) => {
  const ctx = useModal()
  if (!ctx.modal) return false
  const Comp = types[ctx.modal]
  if (!Comp) {
    console.warn('Unknown modal type:', ctx.modal)
    return false
  }

  let closeButton
  if (children && typeof (children) === 'function') {
    closeButton = children(ctx.hide)
  } else if (children) {
    closeButton = children
  } else {
    closeButton = <div className="modal-close" onClick={ctx.hide} />
  }

  const nonDismissible = ctx.options && ctx.options.nonDismissible

  return (
    <div id={ctx.modal} className="modal-background" onClick={!nonDismissible && ctx.hide}>
      <div className="modal-foreground" onClick={e => e.stopPropagation()}>
        {!nonDismissible && closeButton}
        <Comp {...ctx.options} />
      </div>
    </div>
  )
}

Modal.propTypes = {
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node])
}

export const ModalLink = ({ tagName = 'a', children, modal, ...options }) => {
  const ctx = useModal()
  const handleClick = () => ctx.show(modal, options)
  return React.createElement(tagName, { onClick: handleClick }, children)
}
ModalLink.propTypes = {
  tagName: PropTypes.string,
  children: PropTypes.node,
  modal: PropTypes.string.isRequired
}

export const CloseModal = ({ tagName = 'a', children, ...other }) => {
  const ctx = useModal()
  return React.createElement(tagName, { onClick: ctx.hide }, children)
}

export const useModal = () => useContext(ModalContext)[0]
