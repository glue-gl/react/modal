import { ComponentType } from 'react';

export function registerModal(name: string, component: ComponentType<any>): void;

export function ModalWrapper({ children }: {
    children: React.ReactNode;
}): any;

export function ManualModalWrapper({ children, ctx }: {
    children: React.ReactNode;
    ctx: any;
}): any;

export function Modal(): any;

export function ModalLink({ tagName, children, modal, ...options }: {
    [x: string]: any;
    tagName?: string;
    children: React.ReactNode;
    modal: any;
}): any;

export function CloseModal({ tagName, children, ...other }: {
    [x: string]: any;
    tagName?: string;
    children: React.ReactNode;
}): any;

export function useModal(): any;
