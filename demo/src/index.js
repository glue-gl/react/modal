import React, {Component} from 'react'
import {render} from 'react-dom'

import { registerModal, ModalWrapper, Modal, ModalLink, CloseModal } from '../../src'
import '../../css/modal.css'

const ModalA = () => <div>A</div>
const ModalB = ({ mode }) =>
  <div>
    <p>B - {mode}</p>
    <CloseModal tagName="button">Close</CloseModal>
  </div>
registerModal('a', ModalA)
registerModal('b', ModalB)

const Demo = () => {
  return <div>
    <h1>Modal Demo</h1>
    <ModalWrapper>
      <div>
        Pick a modal:<br />
        <ModalLink tagName="button" modal="a">Modal A</ModalLink>
        <ModalLink tagName="button" modal="b" mode={1}>Modal B - opt1</ModalLink>
        <ModalLink tagName="button" modal="b" mode={2} nonDismissible>Modal B - opt2</ModalLink>
        <ModalLink tagName="button" modal="c">Modal C</ModalLink>
        <CloseModal tagName="button">Close</CloseModal>
      </div>
      <div>
        Modal is shown below:
        <Modal />
      </div>
    </ModalWrapper>
  </div>
}

render(<Demo/>, document.querySelector('#demo'))
